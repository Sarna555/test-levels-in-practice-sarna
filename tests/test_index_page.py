import requests
import config
import allure


@allure.suite("Index page tests")
class TestIndexPage:
    """Tests for Index Page:"""

    @allure.title("Index page content verification")
    def test_index_page_is_loaded(self):
        """Verify index page loaded:"""
        response = requests.get(f"{config.base_url}")
        assert response.status_code == 200
        assert "Hello, World!" in response.text
