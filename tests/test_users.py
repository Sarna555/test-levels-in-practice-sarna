import requests
import endpoints


class TestUsers:
    """Tests for /users:"""

    def test_get_users_list(self, new_user_data):
        """Verify creating user"""
        user_list_response = requests.get(endpoints.users)
        assert user_list_response.status_code == 200
        assert new_user_data in user_list_response.json()

    def test_create_user_and_get_thier_details(self, new_user_data):
        """Verify getting user with correct data"""
        created_user_id = new_user_data["id"]

        user_details_response = requests.get(endpoints.user_with_id(created_user_id))

        assert user_details_response.status_code == 200
        assert user_details_response.json() == new_user_data
